# php-integrator/core
<p align="right">
:coffee:
<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YKTNLZCRHMRTJ">Send me some coffee beans</a>
</p>

PHP Integrator is a server that indexes PHP code and performs static analysis. It stores its information in a database
and can retrieve information about your code to clients by communicating over sockets. Clients can use this information
to provide various functionalities, such as autocompletion, code navigation and tooltips.

More information for users, both developers looking to implement the core in other editors as well as programmers using it via editors and IDE's, can be found [on the wiki](https://gitlab.com/php-integrator/core/wikis/home) as well as [the website](https://php-integrator.github.io/).

## Where is it used?
Currently the core package is used to power the php-integrator-* packages for the Atom editor. See also
[the list of projects](https://github.com/php-integrator).

![GPLv3 Logo](/uploads/85ac80ab31bedd5e4622bc5d5484bc02/gplv3-127x51.png)
