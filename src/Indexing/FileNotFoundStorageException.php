<?php

namespace PhpIntegrator\Indexing;

/**
 * Storage exception that indicates a file wasn't found.
 */
class FileNotFoundStorageException extends StorageException
{

}
