<?php

namespace PhpIntegrator\Utility\Typing;

/**
 * Represents a special (non-class) type.
 *
 * {@inheritDoc}
 */
class SpecialType extends Type
{

}
