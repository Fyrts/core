<?php

namespace PhpIntegrator\Analysis\SourceCodeReading;

use RuntimeException;

/**
 * Indicates something went wrong whilst reading source code for a file.
 */
class FileSourceCodeReaderException extends RuntimeException
{

}
